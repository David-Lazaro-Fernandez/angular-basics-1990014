import {Component,Input,OnDestroy,OnInit} from "@angular/core";
import { Subscription } from 'rxjs';
import { PersonajeService } from './personajes.service';

@Component({
    selector: "app-personajes",//aqui designamos el tag del componente 
    templateUrl: './personajes.component.html', //en este archivo ponemos la estructura de nuestro componente
    styleUrls: ['./personajes.component.css'] //aqui cargamos los estilos que utilizara nuestro componente
   })

export class PersonajesComponent implements OnDestroy,OnInit{
    //Declaramos una variable donde escucharemos los cambio de estado de nuestro service
    private personajesSubs: Subscription = new Subscription;

    //declaramos una variable booleana para control de despliegue
    activo: boolean = true;
    consultando: boolean = true;//agregamos esta variable de control para saber cuano termina de cargar
    //declaramos un listado de personajes
    @Input() personajes: string[] = ['Ironman', 'Hulk', 'Batman', 'Superman',"Ant-Man","Aquaman"
    ,"Asterix"
    ,"The Atom"
    ,"The Avengerws"
    ,"Batgirl"
    ,"Batman"
    ,"Batwoman"
    ,"Black Canary"
    ,"Black Panther"
    ,"Captain America"
];


 constructor(private service: PersonajeService){ }
 ngOnInit(){
 this.service.fetchPersonajes();//cambiamos el metodo del servicio por la consulta del api
 this.personajesSubs = this.service.personajesChange.subscribe(personajes => {
 this.personajes = personajes;
 this.consultando = false;//cambiamos el estus de la variable una vez que nos haya regresado la informacion
 });
 }
 ngOnDestroy(){
 this.personajesSubs.unsubscribe();
 }
 onRemovePersonaje(name: string){
 this.service.removePersonaje(name);
 }
 onClickActivar(){
 this.activo = !this.activo;
 }

}
import {Component, Output, EventEmitter} from '@angular/core';
import { PersonajeService } from '../personajes.service';
@Component({
 selector: 'app-personaje-input',
 templateUrl: 'personaje-input.component.html',
 styleUrls: ['personaje-input.component.css']
})
export class PersonajeInputComponent{
 //Por aqui declaramos esta variable para enviar un evento propio
 // y denimos el tipo que en este caso es de tipo string
 
 nombre: string ='';
 //Declaramos nuestro constructor y ponemos como parametro nuestro service para usarlo en la clase
 constructor(private service: PersonajeService){}

 alta(){
 //Usamos el metodo para agregar nuestro personaje con nuestro servicio
 this.service.addPersonaje(this.nombre);
 this.nombre ='';
 }
}
import {Injectable} from '@angular/core';
import { Subject } from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable({providedIn: 'root'})

export class PersonajeService{
    personajes: string[] = [];
    personajesChange = new Subject<string[]>();
    //Creamos el constructor con un parametro de httpCLiente para usarlo dentro de la clase
    constructor(private http: HttpClient){}
    fetchPersonajes(){
    this.http.get<any>('https://swapi.dev/api/people/')
    .pipe(map(response => {//aqui mapeamos la respuesta del servicio y solo obtenemos el campo "name"
    return response.results.map(obj => obj.name);
    }))
    .subscribe(response => {//aqui cuando se terine la consulta y el mapeo asigna los valores a nuestro arreglo
    console.warn(response);
    this.personajes = response;
    this.personajesChange.next(this.personajes);
    });
    }

 addPersonaje(name: string){//Metodo de alta de personaje
 this.personajes.push(name);
 this.personajesChange.next(this.personajes);
 }

 removePersonaje(name: string){//Metodo de baja de personaje
 this.personajes = this.personajes.filter(personaje => {//aplicamos un ltro para devolver todos los personajes menos el
 return personaje !== name;
 });
 this.personajesChange.next(this.personajes);
 }
}
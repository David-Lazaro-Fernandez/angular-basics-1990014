import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {PersonajesComponent} from './personajes/personajes.component';
import {PersonajeInputComponent} from './personajes/personaje-input/personaje-input.component'

const routes: Routes = [
  {path: "", component: PersonajesComponent},
  {path: 'input', component: PersonajeInputComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
